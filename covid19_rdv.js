const yargs = require('yargs');
const argv = yargs
   .usage('$0 <city> [args]')
   .command('city', 'the city to check', {
       city: {
           description: 'the city to check',
           type: 'string',
       }
   })
   .option('strict', {
       alias: 's',
       description: 'enable scrict location',
       type: 'boolean',
   })
   .help()
   .alias('help', 'h')
   .argv;

if (typeof argv._[0] === 'undefined') {
    yargs.showHelp("log");
    return 1;
} else {
   var city = argv._[0];
}

if (typeof argv.strict === 'undefined') {
    var city_strict = false;
} else {
    var city_strict = true;
}

const puppeteer = require('puppeteer');
const search_filter = '?ref_visit_motive_ids[]=6970&ref_visit_motive_ids[]=7005&force_max_limit=2';

(async () => {
    const browser = await puppeteer.launch({
        headless: true
    });
    const page = await browser.newPage();
    await page.goto('https://www.doctolib.fr/vaccination-covid-19/' + city + search_filter);
    await page.setViewport({
        width: 1400,
        height: 800
    });

    await autoScroll(page);

    const rdv = await page.$$eval('div.Tappable-inactive.availabilities-slot', anchors => { return anchors.map(anchor => anchor.ariaLabel) });
    const center = await page.$$eval('div.Tappable-inactive.availabilities-slot', anchors => { return anchors.map(anchor => anchor.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName('dl-search-result-title')[0].innerText.replace('\n', '')) });
    const link = await page.$$eval('div.Tappable-inactive.availabilities-slot', anchors => { return anchors.map(anchor => anchor.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName('dl-search-result-title')[0].childNodes[0].childNodes[0].href) });
    const ville = await page.$$eval('div.Tappable-inactive.availabilities-slot', anchors => { return anchors.map(anchor => anchor.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName('dl-search-result-content')[0].childNodes[1].childNodes[0].innerHTML.replace('<br>', '').normalize("NFD").replace(/[\u0300-\u036f]/g, "")) });
    const address = await page.$$eval('div.Tappable-inactive.availabilities-slot', anchors => { return anchors.map(anchor => anchor.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode.getElementsByClassName('dl-search-result-content')[0].childNodes[1].childNodes[0].innerHTML.replace('<br>', '').split(" ").slice(-1)[0].normalize("NFD").replace(/[\u0300-\u036f]/g, "")) });

    const regex = new RegExp(city, 'gi');

    for (var i = 0; i < rdv.length; i++) {
        if (city_strict) {
            if ( address[i].match(regex) === null ) continue;
        }
        console.log('- ' + rdv[i]);
        console.log('  ' + center[i]);
        console.log('  ' + ville[i]);
        console.log('  ' + link[i]);
    }

    await browser.close();
})();

async function autoScroll(page){
    await page.evaluate(async () => {
        await new Promise((resolve, reject) => {
            var totalHeight = 0;
            var distance = 100;
            var timer = setInterval(() => {
                var scrollHeight = document.body.scrollHeight;
                window.scrollBy(0, distance);
                totalHeight += distance;

                if(totalHeight >= scrollHeight){
                    clearInterval(timer);
                    resolve();
                }
            }, 100);
        });
    });
}
