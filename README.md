## Prerequisite

* `nodejs`
* `yarn`
* for fedora :
```
$ sudo dnf install nodejs yarnpkg
```

## Installation

```
$ git clone git@gitlab.com:jurav/doctolib-covid19-appointment-scraping.git
$ cd doctolib-covid19-appointment-scraping
$ yarn install
yarn install v1.22.10
[1/4] Resolving packages...
[2/4] Fetching packages...
[3/4] Linking dependencies...
[4/4] Building fresh packages...
Done in 5.49s.
```

## Usage

```
$ node covid19_rdv.js
covid19_rdv.js <city> [args]

Commands:
  covid19_rdv.js city  the city to check

Options:
      --version  Show version number                                   [boolean]
  -s, --strict   enable scrict location                                [boolean]
  -h, --help     Show help                                             [boolean]
```

## Example

```
$ node covid19_rdv.js -s montpellier
- lun. 17 mai 14:25
  Centre de vaccination Covid-19 - Hôtel de ville Montpellier Centre de santé
  1 Place Georges Frêche 34000, 34980 Montpellier
  https://www.doctolib.fr/centre-de-sante/montpellier/centre-de-vaccination-hotel-de-ville-montpellier?highlight%5Bspeciality_ids%5D%5B%5D=5494
```
